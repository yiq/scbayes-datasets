Overview
========

This archive contains scripts to build scBayes and its dependencies, as well as
data used to generate the figures in the manuscript.

NOTE: We tested our scripts on the following Linux operating systems:

  * Ubuntu 20.04
  * Red Hat Enterprise Linux 8 or CentOS 8

Other Linux operating systems will probably work. We do not currently support
windows or Mac OS. It may or may not work.

Evaluating scBayes
------------------

scBayes can either be compiled locally (see Building scBayes below) or run
directly via our pre-built docker image `qiaoy/scbayes` available on dockerhub.
Examples are included inside the `dataset` subdirectories. For example, to
perform the subclone assignment for the longitudinal breast cancer patient
dataset, see `dataset/BC/run_scbayes_docker.sh` for how to run scbayes
via docker, or `dataset/BC/run_scbayes.sh` for how to run locally
compiled scbayes. Expected results can be found at
`dataset/BC/expected_results`. The same is true for `dataset/CLL` and
`dataset/CMML`. The expected run time for each dataset is less than 1 minute.

In addition, we organized our R scripts to plot scBayes assignment results into
the `Rscripts` subdirectory within each dataset. Once the cell assignment is
performed successfully, one can navigate into the `Rscripts` directory and 
execute the R scripts, either via `R CMD BATCH <script_file_name>`, or start
the R environment and then source the R script files. The scripts require that
the R libraries `reshape` and `ggplot2` be installed. We tested our scripts
with R version 3.5 and 4.0. When successfully run, multiple PDF files will be
created, which should correspond to parts of the figures we included in our
manuscript.


Building scBayes
----------------

The script `build.sh` attempts to automatically download and build scBayes and
its dependencies, which includes
[htslib](https://github.com/samtools/htslib),
[yaml-cpp](https://github.com/jbeder/yaml-cpp), and scBayes itself. Note that a
c++ compiler with c++14 standard support is necessary. A complete copy of
scBayes code is also made available inside the directory `scbayes`.

System requirements
-------------------

scBayes is designed to be compiled and run from a Linux operating system
environment. It is developed in OpenSUSE 15.2, and tested on Ubuntu 20.04 and
CentOS 8. Other Linux operating systems will probably work

htslib and yaml-cpp have their own dependencies that needs to be satisfied in
order to compile. Here is a list of software components that need to be
available:

  * standard tools including `make` and `git`
  * g++ compiler with c++14 support (tested with gnu c++ v10)
  * htslib needs libz development package (usually named as `zlib-dev` or
    `libz-devel`)
  * yaml-cpp needs `cmake`

The script will first create a directory `build`, in which all downloading and
compilation will take place. If any error were to occur and you wish to try
again, you can simply delete the entire directory. Another directory `local`
inside the current directory will contain the final libraries and binaries. The
two components of scbayes can be found at `local/bin/scGenotype` and
`local/bin/scAssign`.

The typical compilation on a "normal" desktop computer is about 5 minutes


Datasets
--------

We included the datasets used for generating the results in our manuscript.
They are further separated by disease type listed below. Note that due to
updated algorithm / analysis software and float point arithmetic inaccuracies,
small assignment errors (around 2%) are normal, and does not affect the
biological conclusions.

### Synthetic validation dataset from published scDNAseq + pseudobulk results (Supplemental Figure 3)

Data used to generate Supplemental Figure 3 can be found at `dataset/scDNA-pseudoBulk-Validation`

This directory contains the single cell genotype information from the 3 samples
as published in Laks E. et al Cell 2019 (PMID: `31730858`), as well as the VCF
files with the variants of each subclones parsed from the original publication.
The subclone structure and prior probabilities of each sample (`sa921.yaml`,
`sa922.yaml`, and `sa1090.yaml`) are provided to perform cell assignment task.
Results can be visualized using the `tabulate.sh` script, presented in the same
format as Supplemental Figure 3. We put our analysis results in the
`expected_results` subdirectory so you can compare results from your run to
ours. There is a separate `tabulate.sh` script inside the `expected_results`
directory for tabulating and visualizing results.

### Synthetic validation dataset from 10X scRNAseq (Supplemental Figure 4)

Data used to generate Supplemental Figure 4 can be found at `dataset/Synthetic-Validation`

This directory contains the single cell genotype information
(`pt1.bcells.genotype`, etc) for the b cells from three CLL patients. The
subclone structure (`synthetic.assign.yml`) corresponds to the one outlined in
Supplemental Figure 4B. Scripts (`run_scbayes.sh` and `run_scbayes_docker.sh`)
are provided to demonstrate how to perform the cell assignment task. Results
can be visualized using the `tabulate.sh` script in the same format as
Supplemental Figure 4C. We put our analysis results in the `expected_results`
subdirectory so you can compare results from your run to ours. There is a
separate `tabulate.sh` script inside the `expected_results` directory for
tabulating and visualizing results.


### Breast cancer (Figure 2)

Data used to generate figure 2 can be found at `dataset/BC`.

This directory contains the single cells genotype information (`pre.genotype`
for pretreatment cells, and `post.genotype` for post treatment cells) as well
as the subclone structure (`*.yml`) and the subclone defining variants
(`*.vcf`). Scripts (`run_scbayes.sh` and `run_scbayes_docker.sh`) are provided
to demonstrate how to perform the cell assginment task. `tabulate.sh` script
and `expected_results` subdirectory are also provided

### Chronic Lymphocytic Leukemia (Figure 3)

Data used to generate figure 3 can be found at `dataset/CLL`.

Similar to breast cancer, this directory contains single cell genotypes,
subclone structures, subclone defining variants, and a sample script to carry
out cell assignment. `tabulate.sh` script and `expected_results` subdirectory
are also provided.

### Chronic MyeloMonocytic Leukemia (Supplemental Figure 12)

Data used to generate Supplemental Figure 6 can be found at `dataset/CMML`.

This directory contains the single cells genotype information from scATACseq
experiment we performed on a CMML patient sample, as well as the subclone
structure, subclone defining variants, and example scripts to perform the cell
assignment. `tabulate.sh` script and `expected_results` subdirectory are also
provided

More documentation
------------------

Additional documentation is available at the scBayes repository,
[scBayes](https://gitlab.com/yiq/scbayes)
