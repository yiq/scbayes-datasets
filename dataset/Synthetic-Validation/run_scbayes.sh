#!/bin/bash

project_root=$(readlink -f ../../)
SC_ASSIGN=$project_root/local/bin/scAssign

if [ ! -e $SC_ASSIGN ]; then
    echo "scAssign is not found at $project_root/local/bin."
    echo "did you successfully compile scBayes using $project_root/build.sh?"
    exit 0
fi

if [ -e $project_root/local/lib/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib
elif [ -e $project_root/local/lib64/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib64
else
    echo "Cannot find htslib shared library in $project_root/local"
    echo "did you successfully compile htslib using $project_root/build.sh?"
    exit 0
fi

export LD_LIBRARY_PATH=$LD_PREPEND:$LD_LIBRARY_PATH

echo "Performing patient 1 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt1.bcell.genotype > pt1.assign_results.tsv
echo "Done. Results written to pt1.assign_results.tsv"
echo ""

echo "Performing patient 2 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt2.bcell.genotype > pt2.assign_results.tsv
echo "Done. Results written to pt2.assign_results.tsv"
echo ""

echo "Performing patient 3 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt3.bcell.genotype > pt3.assign_results.tsv
echo "Done. Results written to pt3.assign_results.tsv"
echo ""

