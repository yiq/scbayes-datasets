#!/bin/bash

project_root=$(readlink -f ../../../)
SC_ASSIGN="docker run -t -v$(pwd):/tmp -w/tmp --rm qiaoy/scbayes scAssign"

# sometimes docker output is in Windows newline style \0d\0a
# this removes the \0d to make the result comparable to linux outputs
NEWLINE_FIX="sed -e s/\r//g"

echo "Performing patient 1 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt1.bcell.genotype | $NEWLINE_FIX > pt1.assign_results.tsv
echo "Done. Results written to pt1.assign_results.tsv"
echo ""

echo "Performing patient 2 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt2.bcell.genotype | $NEWLINE_FIX > pt2.assign_results.tsv
echo "Done. Results written to pt2.assign_results.tsv"
echo ""

echo "Performing patient 3 cells assignment ..."
$SC_ASSIGN synthetic.assign.yml pt3.bcell.genotype | $NEWLINE_FIX > pt3.assign_results.tsv
echo "Done. Results written to pt3.assign_results.tsv"
echo ""

