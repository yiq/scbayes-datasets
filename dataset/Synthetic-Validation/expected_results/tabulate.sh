#!/bin/bash

PT1="grep -v ^Barcode | grep pt1 | wc -l"
PT2="grep -v ^Barcode | grep pt2 | wc -l"
PT3="grep -v ^Barcode | grep pt3 | wc -l"
NOR="grep -v ^Barcode | grep normal | wc -l"
UNA="grep -v ^Barcode | grep UNASSIGN | wc -l"

PT1_GEN="cat pt1.assign_results.tsv"
PT2_GEN="cat pt2.assign_results.tsv"
PT3_GEN="cat pt3.assign_results.tsv"

echo -e SourcePt'\t'ToPt1'\t'ToPt2'\t'ToPt3'\t'ToNorm'\t'Unassigned
GEN=$PT1_GEN
echo -e patient1'\t'$(eval "$GEN | $PT1")'\t'$(eval "$GEN | $PT2")'\t'$(eval "$GEN | $PT3")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
GEN=$PT2_GEN
echo -e patient2'\t'$(eval "$GEN | $PT1")'\t'$(eval "$GEN | $PT2")'\t'$(eval "$GEN | $PT3")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
GEN=$PT3_GEN
echo -e patient3'\t'$(eval "$GEN | $PT1")'\t'$(eval "$GEN | $PT2")'\t'$(eval "$GEN | $PT3")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
