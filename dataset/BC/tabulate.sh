#!/bin/bash

SC1="grep -v ^Barcode | grep SC1 | wc -l"
SC2="grep -v ^Barcode | grep SC2 | wc -l"
SC3="grep -v ^Barcode | grep SC3 | wc -l"
SC4="grep -v ^Barcode | grep SC4 | wc -l"
SC5="grep -v ^Barcode | grep SC5 | wc -l"

PRE_GEN="cat pre.assign_results.tsv"
POST_GEN="cat post.assign_results.tsv"

echo -e Sample'\t'SC1'\t'SC2'\t'SC3'\t'SC4'\t'SC5
GEN=$PRE_GEN
echo -e pre'\t'$(eval "$GEN | $SC1")'\t'$(eval "$GEN | $SC2")'\t'$(eval "$GEN | $SC3")'\t'$(eval "$GEN | $SC4")'\t'$(eval "$GEN | $SC5")
GEN=$POST_GEN
echo -e post'\t'$(eval "$GEN | $SC1")'\t'$(eval "$GEN | $SC2")'\t'$(eval "$GEN | $SC3")'\t'$(eval "$GEN | $SC4")'\t'$(eval "$GEN | $SC5")
