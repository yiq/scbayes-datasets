#!/bin/bash

project_root=$(readlink -f ../../)
SC_ASSIGN=$project_root/local/bin/scAssign

if [ ! -e $SC_ASSIGN ]; then
    echo "scAssign is not found at $project_root/local/bin."
    echo "did you successfully compile scBayes using $project_root/build.sh?"
    exit 0
fi

if [ -e $project_root/local/lib/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib
elif [ -e $project_root/local/lib64/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib64
else
    echo "Cannot find htslib shared library in $project_root/local"
    echo "did you successfully compile htslib using $project_root/build.sh?"
    exit 0
fi

export LD_LIBRARY_PATH=$LD_PREPEND:$LD_LIBRARY_PATH

echo "Performing pre-treatment cells assignment ..."
$SC_ASSIGN pre.assign.yml pre.genotype > pre.assign_results.tsv
echo "Done. Results written to pre.assign_results.tsv"
echo ""

echo "Performing post-treatment cells assignment ..."
$SC_ASSIGN post.assign.yml post.genotype > post.assign_results.tsv
echo "Done. Results written to post.assign_results.tsv"
