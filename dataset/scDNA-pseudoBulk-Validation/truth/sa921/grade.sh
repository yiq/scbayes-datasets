#!/bin/bash

SAMPLE=sa921
TRUTH_CLONES="A B C D E F G H I"
ASSIG_CLONES="$TRUTH_CLONES normal UNASSIGN"

echo -n "assign"

for truth in $TRUTH_CLONES; do
    echo -n -e "\t$truth"
done
echo ""

for assigned in $ASSIG_CLONES; do
    echo -n $assigned | sed 's/UNASSIGN/UNASSI/'
    for truth in $TRUTH_CLONES; do
        count=$(awk "\$2==\"$assigned\"" $1 | grep -f truth/$SAMPLE/$truth.cells.txt | wc -l)
        echo -e -n "\t$count"
    done
    echo ""
done
    

