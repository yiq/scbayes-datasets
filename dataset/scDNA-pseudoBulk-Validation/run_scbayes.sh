#!/bin/bash

project_root=$(readlink -f ../../)
SC_ASSIGN=$project_root/local/bin/scAssign

if [ ! -e $SC_ASSIGN ]; then
    echo "scAssign is not found at $project_root/local/bin."
    echo "did you successfully compile scBayes using $project_root/build.sh?"
    exit 0
fi

if [ -e $project_root/local/lib/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib
elif [ -e $project_root/local/lib64/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib64
else
    echo "Cannot find htslib shared library in $project_root/local"
    echo "did you successfully compile htslib using $project_root/build.sh?"
    exit 0
fi

export LD_LIBRARY_PATH=$LD_PREPEND:$LD_LIBRARY_PATH

echo "Performing SA921 cells assignment ..."
$SC_ASSIGN sa921.yaml ov2295_sa921_genotype.tsv > sa921.assign_results.tsv
echo "DONE. results written to sa921.assign_results.tsv"
echo ""
echo "Performing SA922 cells assignment ..."
$SC_ASSIGN sa922.yaml ov2295_sa922_genotype.tsv > sa922.assign_results.tsv
echo "DONE. results written to sa922.assign_results.tsv"
echo ""
echo "Performing SA1090 cells assignment ..."
$SC_ASSIGN sa1090.yaml ov2295_sa1090_genotype.tsv > sa1090.assign_results.tsv
echo "DONE. results written to sa1090.assign_results.tsv"
echo ""


