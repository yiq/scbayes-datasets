#!/bin/bash

echo "---- SA921 ASSIGNMENT ----"
./grade.sh sa921.assign_results.tsv sa921
echo "---- SA921 RESULT END ----"
echo ""

echo "---- SA922 ASSIGNMENT ----"
./grade.sh sa922.assign_results.tsv sa922
echo "---- SA922 RESULT END ----"
echo ""

echo "---- SA1090 ASSIGNMENT ----"
./grade.sh sa1090.assign_results.tsv sa1090
echo "---- SA1090 RESULT END ----"
