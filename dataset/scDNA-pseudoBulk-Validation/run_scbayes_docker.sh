#!/bin/bash

project_root=$(readlink -f ../../../)
SC_ASSIGN="docker run -t -v$(pwd):/tmp -w/tmp --rm qiaoy/scbayes scAssign"

# sometimes docker output is in Windows newline style \0d\0a
# this removes the \0d to make the result comparable to linux outputs
NEWLINE_FIX="sed -e s/\r//g"

echo "Performing SA921 cells assignment ..."
$SC_ASSIGN sa921.yaml ov2295_sa921_genotype.tsv | $NEWLINE_FIX > sa921.assign_results.tsv
echo "Performing SA922 cells assignment ..."
$SC_ASSIGN sa922.yaml ov2295_sa922_genotype.tsv | $NEWLINE_FIX > sa922.assign_results.tsv
echo "Performing SA1090 cells assignment ..."
$SC_ASSIGN sa1090.yaml ov2295_sa1090_genotype.tsv | $NEWLINE_FIX > sa1090.assign_results.tsv

#echo "Performing pre-treatment cells assignment ..."
#$SC_ASSIGN -q 5 pre.assign.yml pre.genotype | $NEWLINE_FIX > pre.assign_results.tsv
#echo "Done. Results written to pre.assign_results.tsv"
#echo ""
#
#echo "Performing post-treatment cells assignment ..."
#$SC_ASSIGN -q 5 post.assign.yml post.genotype | $NEWLINE_FIX > post.assign_results.tsv
#echo "Done. Results written to post.assign_results.tsv"
#echo ""
