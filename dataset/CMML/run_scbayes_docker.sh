#!/bin/bash

project_root=$(readlink -f ../../../)
SC_ASSIGN="docker run -t -v$(pwd):/tmp -w/tmp --rm qiaoy/scbayes scAssign"

# sometimes docker output is in Windows newline style \0d\0a
# this removes the \0d to make the result comparable to linux outputs
NEWLINE_FIX="sed -e s/\r//g"

echo "Performing scATACseq cells assignment ..."
$SC_ASSIGN atac.assign.yml atac.genotype | $NEWLINE_FIX > atac.assign_results.tsv
echo "Done. Results written to atac.assign_results.tsv"
echo ""
