#!/bin/bash

TUMOR="grep -v ^Barcode | grep tumor | wc -l"
NOR="grep -v ^Barcode | grep normal| wc -l"
UNA="grep -v ^Barcode | grep UNASSIGN| wc -l"

ATAC_GEN="cat atac.assign_results.tsv"

echo -e Sample'\t'tumor'\t'normal'\t'Unassigned
GEN=$ATAC_GEN
echo -e CMML'\t'$(eval "$GEN | $TUMOR")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
