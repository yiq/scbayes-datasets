#!/bin/bash

project_root=$(readlink -f ../../)
SC_ASSIGN=$project_root/local/bin/scAssign

if [ ! -e $SC_ASSIGN ]; then
    echo "scAssign is not found at $project_root/local/bin."
    echo "did you successfully compile scBayes using $project_root/build.sh?"
    exit 0
fi

if [ -e $project_root/local/lib/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib
elif [ -e $project_root/local/lib64/libhts.so ]; then
    LD_PREPEND=$project_root/local/lib64
else
    echo "Cannot find htslib shared library in $project_root/local"
    echo "did you successfully compile htslib using $project_root/build.sh?"
    exit 0
fi

export LD_LIBRARY_PATH=$LD_PREPEND:$LD_LIBRARY_PATH

echo "Performing T1 cells assignment ..."
$SC_ASSIGN T1.assign.yml T1.bcell.genotype > T1.assign_results.tsv
echo "Done. Results written to T1.assign_results.tsv"
echo ""

echo "Performing T2 cells assignment ..."
$SC_ASSIGN T2.assign.yml T2.bcell.genotype > T2.assign_results.tsv
echo "Done. Results written to T2.assign_results.tsv"
echo ""

echo "Performing T3 cells assignment ..."
$SC_ASSIGN T3.assign.yml T3.bcell.genotype > T3.assign_results.tsv
echo "Done. Results written to T3.assign_results.tsv"
echo ""
