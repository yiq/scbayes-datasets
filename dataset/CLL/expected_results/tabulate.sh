#!/bin/bash

SC1="grep -v ^Barcode | grep SC1 | wc -l"
SC2="grep -v ^Barcode | grep SC2 | wc -l"
NOR="grep -v ^Barcode | grep normal| wc -l"
UNA="grep -v ^Barcode | grep UNASSIGN| wc -l"

T1_GEN="cat T1.assign_results.tsv"
T2_GEN="cat T2.assign_results.tsv"
T3_GEN="cat T3.assign_results.tsv"

echo -e Sample'\t'SC1'\t'SC2'\t'normal'\t'Unassigned
GEN=$T1_GEN
echo -e T1'\t'$(eval "$GEN | $SC1")'\t'$(eval "$GEN | $SC2")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
GEN=$T2_GEN
echo -e T2'\t'$(eval "$GEN | $SC1")'\t'$(eval "$GEN | $SC2")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
GEN=$T3_GEN
echo -e T3'\t'$(eval "$GEN | $SC1")'\t'$(eval "$GEN | $SC2")'\t'$(eval "$GEN | $NOR")'\t'$(eval "$GEN | $UNA")
