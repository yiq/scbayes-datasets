#!/bin/bash

project_root=$(readlink -f ../../../)
SC_ASSIGN="docker run -t -v$(pwd):/tmp -w/tmp --rm qiaoy/scbayes scAssign"

# sometimes docker output is in Windows newline style \0d\0a
# this removes the \0d to make the result comparable to linux outputs
NEWLINE_FIX="sed -e s/\r//g"

echo "Performing T1 cells assignment ..."
$SC_ASSIGN T1.assign.yml T1.bcell.genotype | $NEWLINE_FIX > T1.assign_results.tsv
echo "Done. Results written to T1.assign_results.tsv"
echo ""

echo "Performing T2 cells assignment ..."
$SC_ASSIGN T2.assign.yml T2.bcell.genotype | $NEWLINE_FIX > T2.assign_results.tsv
echo "Done. Results written to T2.assign_results.tsv"
echo ""

echo "Performing T3 cells assignment ..."
$SC_ASSIGN T3.assign.yml T3.bcell.genotype | $NEWLINE_FIX > T3.assign_results.tsv
echo "Done. Results written to T3.assign_results.tsv"
echo ""
